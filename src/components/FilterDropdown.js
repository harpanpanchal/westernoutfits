import React from "react";
import _ from "lodash";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import styled from "styled-components";

const DropdownOption = styled.select`
  width: 117px;
  color: #444;
  line-height: 1.3;
  padding: 0.6em 0.4em 0.5em 0.8em;
  box-sizing: border-box;
  margin: 0;
  border: 1px solid #aaa;
  border-radius: 0.5em;
  -moz-appearance: none;
  -webkit-appearance: none;
  appearance: none;
  background-color: #fff;
  background-repeat: no-repeat, repeat;
  background-position: right 0.5em top 50%, 0 0;
  background-size: 0.65em auto, 100%;

  &::-ms-expand {
    display: none;
  }
`;

class FilterDropdown extends React.Component {
  constructor(props) {
    super(props);
  }

  renderList() {
    const newArray = this.props.itemlist.map(item => item.size);
    let finalArray = [];
    newArray.forEach(function(element) {
      finalArray.push(...element);
    });
    return _.union(finalArray).map(item => (
      <option key={item} value={item}>{item}</option>
    ));
  }
  render() {
    return (
      <div>
        <DropdownOption className="filters" onChange={this.props.filter}>
          <option key="Please select option" value="Please select option">Filter by size</option>
          {this.renderList()}
        </DropdownOption>
      </div>
    );
  }
}

FilterDropdown.propTypes = {
  filter: PropTypes.func.isRequired,
  itemlist: PropTypes.array
};

const mapStateToProps = state => {
  return {
    itemlist: state.itemlist
  };
};

export default connect(mapStateToProps)(FilterDropdown);
