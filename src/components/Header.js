import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { searchfilter } from "../actions";
import FilterDropdown from './FilterDropdown';
class Header extends React.Component {
 
  filterData = (e) => {
    this.props.searchfilter(e.target.value);
  }
  render() {
    return (
      <div className="container header">
        <div className="grid-row">
          <div className="grid-item">
            <h1>Women's tops</h1>
          </div>
          <div className="grid-item">
            <form>
              <FilterDropdown filter={this.filterData.bind(this)} />
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    searchfilter: state.searchfilter,
    itemlist: state.itemlist
  };
};

Header.propTypes = {
  searchfilter: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  { searchfilter }
)(Header);
