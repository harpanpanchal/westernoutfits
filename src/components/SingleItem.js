import React from 'react';
import PropTypes from "prop-types";

const SingleItem = (props) => {
return(<div className="grid-item">
<div className="grid-item-wrapper">
  <div className="grid-item-container">
    <div className="grid-image-top">
    <img
          src={`../assets/images/photos/${props.ts.productImage}`}
          className="centered" alt={props.ts.productName}
        />
    </div>
    <div className="grid-item-content">
      <div className="item-exclusive-sale">
        {props.ts.isExclusive ? (
          <span className="item-exclusive">Exclusive</span>
        ) : (
          ""
        )}
        {props.ts.isSale ? <span className="item-sale">SALE</span> : ""}
      </div>

      <span className="item-title">{props.ts.productName}</span>
      <span className="item-price">{props.ts.price}</span>
    </div>
  </div>
</div>
</div>)
};

SingleItem.propTypes = {
  ts: PropTypes.object
};

export default SingleItem;