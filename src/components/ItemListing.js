import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { fetchList } from "../actions";
import SingleItem from './SingleItem';
import _ from "lodash";

class ItemListing extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.fetchList();
  }
  renderList() {
    if (
      !this.props.searchfilter ||
      this.props.searchfilter === "Please select option"
    ) {
      return this.props.itemlist.map(ts => {
        return (
          <SingleItem ts={ts} key={ts.index} />
        );
      });
    } else {
      const sfilter = this.props.searchfilter;
      return this.props.itemlist
        .filter(function(ts) {
          return _.includes(ts.size, sfilter);
        })
        .map(function(ts) {
          return (
            <SingleItem ts={ts} key={ts.index} />
          );
        });
    }
  }
  render() {
    return this.renderList();
  }
}

const mapStateToProps = state => {
  return {
    searchfilter: state.searchfilter,
    itemlist: state.itemlist
  };
};
ItemListing.propTypes = {
  fetchList: PropTypes.func.isRequired,
  searchfilter: PropTypes.string,
  itemlist: PropTypes.array.isRequired,
  ts: PropTypes.object
};

export default connect(
  mapStateToProps,
  { fetchList }
)(ItemListing);
