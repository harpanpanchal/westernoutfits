import React, { Component } from "react";
import Header from "./Header";
import ItemListing from "./ItemListing";
import "../styles/index.scss";
class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <div className="container">
          <div className="grid-row">
            <ItemListing />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
