import { combineReducers } from "redux";

const itemlistReducer = (state = [], action) => {
  switch (action.type) {
    case "FETCH_LIST":
      return action.payload;

    default:
      return state;
  }
};

const searchfilterReducer = (searchfilter = null, action) => {
  if (action.type === "FILTER_SELECTED") {
    return action.payload;
  }
  return searchfilter;
};

export default combineReducers({
  itemlist: itemlistReducer,
  searchfilter: searchfilterReducer
});
