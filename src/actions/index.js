import fetchJsonApi from "../apis/fetchJsonApi";
// Action creator
export const searchfilter = filter => {
  // Return an action
  return {
    type: "FILTER_SELECTED",
    payload: filter
  };
};

export const fetchList = () => async dispatch => {
  const response = await fetchJsonApi.get("/b/5cae9a54fb42337645ebcad3");
  dispatch({
    type: "FETCH_LIST",
    payload: response.data
  });
};
