This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installation guideline

1. Install the application by running 'npm install' in the command prompt / terminal.

2. Once installed, type 'npm start' in the command prompt. You can then view the application in the browser by opening http://localhost:9000 url.

## Technical specifications

1. All the main resource folders / files are located within 'src' folder. Some of them are Actions / Reducers / components / apis, etc.

2. Data has been fetched via API and filter functionality helps filtering the data based on the different sizes.

3. Dropdown filter pulls the data from the fetched API only via store.

4. CSS / Flexbox has been used here and tried to avoid using any CSS framework like Bootstrap / Foundation / Materialize, etc.

5. Application is responsive as well.

6. Styled Component has been used for demo purpose for drop down. The same can be found in its individual component file for the reference.

7. Header / ItemListing are the main components. ItemListing component fetches the single item code to avoid duplicate code / DRY.

8. SASS (CSS Pre-processor) / Webpack (Bundle Moduler) implemented as well.
